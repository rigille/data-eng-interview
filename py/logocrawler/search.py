from urllib.parse import urlparse
from dataclasses import dataclass
from enum import Enum
from typing import Optional
from httpx import URL
from bs4 import BeautifulSoup, Tag

@dataclass
class SearchHomepageLink:
    sitename: str
    def search_tag(self, tag):
        if tag.name == "a" and tag.has_attr('href'):
            href = urlparse(tag['href'])
            same_site = href.netloc == '' or self.sitename in href.netloc
            landing_page = href.path == '' or href.path == '/'
            if tag.has_attr('class'):
                logo_class = "logo" in [c.lower() for c in tag['class']]
            else:
                logo_class = False
            return (same_site and landing_page) or logo_class or tag['href'] == ''
        else:
            return False

def normalize_host(host: str) -> str:
    www = "www."
    if host.startswith("www."):
        return host[len(www):]
    else:
        return host

class LogoKind(Enum):
    IMG = "IMG"
    SVG = "SVG"

@dataclass
class Logo:
    kind: LogoKind
    content: str

def logo(sitename: str, html: str) -> Optional[Logo]:
    soup = BeautifulSoup(html, "html.parser")
    ret = []
    for tag in soup.find_all(SearchHomepageLink(sitename).search_tag):
        ret.append(tag.find_all(["img", "svg"]))
    if len(ret) >= 1:
        if len(ret[0]) == 1:
            return convert_tag(sitename, ret[0][0])
        ret[0] = [item for item in ret[0] if "logo" in str(item).lower()]
        if len(ret[0]) == 1:
            return convert_tag(sitename, ret[0][0])
    return None

def convert_tag(sitename: str, tag: Tag) -> Optional[Logo]:
    if tag.name == "img":
        if tag.has_attr("src"):
            src = tag['src'].strip()
        elif tag.has_attr("data-src"):
            src = tag['data-src'].strip()

        if src.startswith('//'):
            src = "http:" + src
        if not src.startswith('data:image'):
            url_src = URL(src)
            if url_src.host == '':
                separator = '/' if url_src != '' and url_src.path[0] != '/' else ''
                formatted_src = f"http://{sitename}{separator}{src}"
            else:
                formatted_src = src
            return Logo(LogoKind.IMG, formatted_src)
        return None
    elif tag.name == "svg":
        return Logo(LogoKind.SVG, str(tag))
    else:
        return None
