from typing import Optional, List
from dataclasses import dataclass, field

import os
import sys
import asyncio
from asyncio import Semaphore

from httpx import AsyncClient, URL

from logocrawler.search import logo, LogoKind, normalize_host

@dataclass
class RedirectedHTML:
    html: str = field(repr=False)
    redirect: Optional[URL]
    url: URL

def sitename(data: RedirectedHTML) -> str:
    host = data.redirect.host if data.redirect else data.url.host
    return normalize_host(host)

async def get(limiter: Semaphore, session: AsyncClient, url: URL) -> Optional[RedirectedHTML]:
    try:
        async with limiter:
            headers = {
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:107.0) Gecko/20100101 Firefox/107.0',
                'Accept': 'text/html',
                'Accept-Language': 'en-US,en;q=0.5'
            }
            response = await session.get(url, timeout=60, headers=headers)
            redirect = response.url if normalize_host(response.url.host) != normalize_host(url.host) else None
            return RedirectedHTML(response.text, redirect, url)
    except Exception:
        return None

async def main() -> None:
    limiter = Semaphore(50)
    unavailable = 0
    len_0 = 0
    len_1 = 0
    if not os.path.exists("svgs"):
        os.makedirs("svgs")
    async with AsyncClient(http2=True, verify=False) as session:
        urls: List[str] = [line.strip() for line in sys.stdin]
        requests = [get(limiter, session, URL(f"http://{url}")) for url in urls]
        for coro in asyncio.as_completed(requests):
            data = await coro
            if data:
                sn = sitename(data)
                result = logo(sn, data.html)
                if result:
                    len_1 += 1
                    if result.kind == LogoKind.IMG:
                        print(f"{data.url}, {result.content}")
                    elif result.kind == LogoKind.SVG:
                        with open(f"svgs/{sn}.svg", "w") as fp:
                            fp.write(result.content)
                        print(f"{data.url}, file://{os.path.abspath(os.getcwd())}/svgs/{sn}.svg")
                else:
                    len_0 += 1
            else:
                unavailable += 1
        print("unavailable:", unavailable, "or", unavailable/len(urls))
        print("not found:", len_0, "or", len_0/(len(urls) - unavailable))
        print("found:", len_1, "or", len_1/(len(urls) - unavailable))


asyncio.run(main())
