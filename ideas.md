# Http
## Which sites block automated clients?
Only 63.8% of the sites respond to a `requests.get`.

## How to make requests faster?
I've tried `aiohttp` to make the requests concurrently. But it had many errors... likely related to the limits of the pool of tcp connection. With `httpx` and `HTTP/2`, which doesn't need connection pools, it works like a charm. Anyway this takes about 30 seconds and it's not fast enough for tests. So I cached the responses inside files while I developed a strategy to find logos.

## How to handle redirects?
I return the redirect info from the function that does the requests. The csv output uses the same url, even after redirects. This is meant to stay in line with the specification, but I'll ask if that's what's intended.

## How to get around the other 36.2%?
Maybe setting headers and cookies? I did that and now only 13% are forbidden or unavailable.

## Is this legal?
I don't know. Probably yes.

# Logo detection
## What kinds of patterns are more interesting?
The patterns that relate to how the website is used, how the users navigate and the meaning of the information, not coincidences in the character stream.

## How to iterate fast?
Cache the html inside files. The search function is pure, so it doesn't care where the string comes from. This is good for debugging too.

## Are all logos inside img tags?
No! Some are drawn in svg tags. Some also don't appear as neither imgs nor svgs, but if I delete the corresponding html tag, the image disappears. I suspect these are set using `background-image`.

## How to handle svg tags?
I'll save them to file and return a `file://` url. Is it cheating? Haha Technically it's inside the scope. I'll ask about that too. In a real scenario of course I would host it in a proper place.

## How to get logos from `background-image` css attributes?
I don't know, but it would significantly increase the recall.

## Finding homepage links
This worked very well. The idea is finding the first link to the homepage inside the html and then look if there's a single img or svg inside it. Or if it has a single img or svg which contains the word "logo". This has a strong semantics, since it's unlikely a site would use any other logo for a link to it's homepage. I got about of 30% recall, and the results are very precise as you can see by clicking the links. My tests indicate this could double if we rendered the dynamic html as well.

## How to handle dynamic html sites?
I tried to use an html rendering service called splash. Albeit the results were promising in early tests, splash is unstable and sometimes freezes. So I ditched it. The idea of having an html rendering service is very interesting if well executed, might be a good way to scale this. Selenium is more limited in that regard.

## What if the path of the link to the homepage is not empty?
I've witnessed this happen sometimes. For example LG's logo links to lg.com/br instead of lg.com. I haven't found a way to detect these kinds of situations, all my ideas are either too costly or risk losing precision.

# Roadmap
* [x] Define a dataclass that contains the relevant information from the response
* [x] Extract the name of the site from the url
* [x] Try to find the img tag as_completed
* [x] Extract the img url
* [x] Deal with inline svg
* [x] Deal with exceptions from cloudflare
* [x] Configure nix environment
